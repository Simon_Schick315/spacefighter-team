#include "Ufo.h"
#include "conio.h"
#include "Math.h"


Ufo::Ufo(float speed, float MaxHP, int collisionRadius)
{
	//GameTime *pGameTime = new GameTime; // temporarily creating a GameTime object so I can the LastShot function on spawn
	SetSpeed(speed);
	SetMaxHitPoints(MaxHP);
	SetCollisionRadius(collisionRadius);
	//SetInvulnurable();
	//SetLastShot(pGameTime->GetTotalTime());
	SetScoreAddonPoints(45);
	//delete pGameTime; // deleting the GameTime object to avoid memory leaks
}

void Ufo::Update(const GameTime *pGameTime)
{
	if (IsActive())
	{
		float x;
		float y;

		int currentHeight = this->GetPosition().Y;
		
		if ( currentHeight >= Game::GetScreenHeight() * 0.15 )
		{
			//SetSpeed(0);

			SetArrived(true);
		}


		if (!m_arrived) //Ship has not arrived at Y coordinate
		{
			y = GetSpeed() * pGameTime->GetTimeElapsed();
			x = sin(pGameTime->GetTotalTime() * Math::PI + Game::GetScreenCenter().X);
			x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;
		}
		else if (m_arrived) //Ship has arrived at Y coordinate
		{
			y = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex()); 
			y *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;

			x = sin(pGameTime->GetTotalTime() * Math::PI * 0.25f );
			x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;
		}

		//std::cout << x << "\n";

		TranslatePosition(x, y);

		//std::cout << this->GetPosition().X << ", " << this->GetPosition().Y << "\n";
		//std::cout << Game::GetScreenHeight();
		
		/*
		if ((pGameTime->GetTotalTime() - GetLastShot()) >= 5.0 && IsActive()) // shoots projectile after 5 seconds
		{
			
			SetLastShot(pGameTime->GetTotalTime());
			std::cout << "\nUfo shoots weapon\n"; // for debugging purposes
		}
		*/	

		Ship::FireWeapons(1, TriggerType::ALL);

		if (!IsOnScreen()) Deactivate();
	}

	EnemyShip::Update(pGameTime);
}

void Ufo::Teleport()
{
	FireWeapons(1, TriggerType::PRIMARY);

	float x = (float)rand() / (float)RAND_MAX; // Get a random number between 0 and 1.
	

	float teleportRange = x * Game::GetScreenWidth() * 0.7;
	teleportRange += Game::GetScreenWidth() * 0.15; // forces the x coordinate to be at least 1600*0.15, making the range 0.15-0.85 of the screen width.

	//std::cout << Game::GetScreenWidth() << "\n" << teleportRange;

	this->SetPosition(teleportRange, this->GetPosition().Y );
	
}

void Ufo::Hit(const float damage)
{
	Ship::Hit(damage);
	SetScore(5); // adds to score when ship is hit
	//std::cout << "\n" << ToString() << " | Points Added to Score: " << 5 << "\n" << "Overall Score: " << GameObject::GetScore() << "\n";
	if (GetHitPoints() <= 0)
	{
		GameObject::SaveScore();
	}
	Teleport();
}
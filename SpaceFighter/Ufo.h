#pragma once
#include "BossEnemyShip.h"
#include "GameTime.h"
class Ufo : public BossEnemyShip
{
public:

	Ufo(float speed, float MaxHP, int collisionRadius);
	virtual ~Ufo() { }
	
	virtual void Update(const GameTime *pGameTime);

	virtual void Teleport();

	virtual void Hit(const float damage);

	//virtual void SetLastShot(const double lastShot) { m_lastShot = lastShot; }
	//
	//virtual double GetLastShot() { return m_lastShot; }
private:

	//double m_lastShot;
	//Vector2 m_desiredDirection;
};


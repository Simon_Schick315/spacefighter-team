
#pragma once

#include "Weapon.h"
#include "conio.h"

class Blaster : public Weapon
{

public:

	Blaster(const bool isActive, float cooldown = 0, float cooldownSeconds = 0.1, bool isPlayerWeapon = true) : Weapon(isActive)
	{
		m_cooldown = cooldown;
		m_cooldownSeconds = cooldownSeconds; // ajusted the cooldown time

		m_playerWeapon = isPlayerWeapon;
	}

	virtual ~Blaster() { }

	virtual void Update(const GameTime *pGameTime)
	{
		if (m_cooldown > 0) m_cooldown -= pGameTime->GetTimeElapsed();
	}

	virtual bool CanFire() const { return m_cooldown <= 0; }

	virtual void ResetCooldown() { m_cooldown = 0; }

	virtual float GetCooldownSeconds() { return m_cooldownSeconds; }

	virtual void SetCooldownSeconds(const float seconds) { m_cooldownSeconds = seconds; }

	virtual void Fire(int TextureIndexInput, TriggerType triggerType)
	{
		if (IsActive() && CanFire())
		{
			if (triggerType.Contains(GetTriggerType()))
			{
				Projectile *pProjectile = GetProjectile();
				if (pProjectile)
				{
					pProjectile->Activate(GetPosition(), m_playerWeapon, TextureIndexInput);
					m_cooldown = m_cooldownSeconds;
				}
			}
		}
	}


private:

	float m_cooldown;
	float m_cooldownSeconds;
	bool m_playerWeapon;

};
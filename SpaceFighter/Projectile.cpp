
#include "Projectile.h"

Texture *Projectile::s_pTexture[];// = nullptr;

Projectile::Projectile()
{
	SetSpeed(500);
	SetDamage(1);
	SetDirection(-Vector2::UNIT_Y);
	SetCollisionRadius(9);

	m_drawnByLevel = true;
}

Projectile::Projectile(int speed, float damage, Vector2 direction, int collisionRadius)
{
	SetSpeed(speed);
	SetDamage(damage);
	SetDirection(direction);
	SetCollisionRadius(collisionRadius);

	m_drawnByLevel = true;
}

void Projectile::Update(const GameTime *pGameTime)
{
	if (IsActive()) // calls the IsActive function in GameObject.h to access the m_isActive field
	{
		Vector2 translation = m_direction * m_speed * pGameTime->GetTimeElapsed(); // sets the value of a 2D vector by multiplying the projectiles direction by its speed 
																				   // and then multiplying by the elapsetime since last frame
		TranslatePosition(translation); // moves the projectile by the translation variable							

		Vector2 position = GetPosition(); // sets the position variable the the current position of the projectile
		Vector2 size = s_pTexture[m_textureIndex]->GetSize(); // sets the size of the projectile's texture

		// Is the projectile off the screen?
		if (position.Y < -size.Y) Deactivate(); // calls a function if the projectile's Y position is less than 0
		else if (position.X < -size.X) Deactivate(); // calls a function if the projectile's X position is less than 0
		else if (position.Y > Game::GetScreenHeight() + size.Y) Deactivate(); // calls a function if the projectile's Y position is greater than the game screen's height
		else if (position.X > Game::GetScreenWidth() + size.X) Deactivate(); // calls a function if the projectile's X position is greater than the game screen's width
	}

	GameObject::Update(pGameTime); // calls this function to update the position of projectile at the end of every time this function loops
}

void Projectile::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(s_pTexture[m_textureIndex], GetPosition(), Color::White, s_pTexture[m_textureIndex]->GetCenter());
	}
}

void Projectile::Activate(const Vector2 &position, bool wasShotByPlayer, int TextureIndexInput)
{
	m_wasShotByPlayer = wasShotByPlayer; // needs to be false for enemy ship shots
	SetPosition(position);

	m_textureIndex = TextureIndexInput;

	GameObject::Activate();
}

std::string Projectile::ToString() const
{
	return ((WasShotByPlayer()) ? "Player " : "Enemy ") + GetProjectileTypeString();
}

CollisionType Projectile::GetCollisionType() const
{
	CollisionType shipType = WasShotByPlayer() ? CollisionType::PLAYER : CollisionType::ENEMY;
	return (shipType | GetProjectileType());
}
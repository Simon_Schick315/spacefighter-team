
#pragma once

#include "EnemyShip.h"

class BioEnemyShip : public EnemyShip
{

public:
	//constructor/destructor
	BioEnemyShip();
	BioEnemyShip(float speed, float MaxHP, int collisionRadius );
	virtual ~BioEnemyShip() { }

	virtual void SetTexture(Texture *pTexture) { m_pTexture = pTexture; }

	virtual void Update(const GameTime *pGameTime);

	virtual void Draw(SpriteBatch *pSpriteBatch);


private:

	Texture *m_pTexture = nullptr;

};
#pragma once
#include "GameObject.h"
class PowerUp : public GameObject    
{
public:
	PowerUp();
	virtual ~PowerUp() { }

	virtual CollisionType GetCollisionType() const { return (CollisionType::POWERUP | CollisionType::SHIP); }
	
	virtual void Draw(SpriteBatch *pSpriteBatch);

	//virtual void LoadContent(ResourceManager *pResourceManager);

	virtual void Update(const GameTime *pGameTime);

	virtual void Hit(const float damage);

	virtual float GetSpeed() const { return m_speed; }

	virtual void SetSpeed(const float speed) { m_speed = speed; }

	virtual void SetMaxHitPoints(const float hitPoints) { m_maxHitPoints = hitPoints; }

	virtual std::string ToString() const { return "PowerUp"; }

	virtual void SetTexture(Texture *pTexture) { m_pTexture = pTexture; }

	virtual void Initialize(const Vector2 position, const double delaySeconds);

	virtual void SetScoreAddonPoints(const int scorePoints) { m_scorePoints = scorePoints; }

private:

	float m_speed;

	float m_maxHitPoints;

	Texture *m_pTexture = nullptr;

	double m_delaySeconds;

	int m_scorePoints;

	float m_hitPoints;
	//Vector2 m_EnemyPos;

	//enum powerUpType { HealthShield };
};
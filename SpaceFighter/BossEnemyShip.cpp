#include "BossEnemyShip.h"
#include "conio.h"

BossEnemyShip::BossEnemyShip()
{
	SetSpeed(0);
	SetMaxHitPoints(10);
	SetCollisionRadius(160);
}

void BossEnemyShip::Update(const GameTime *pGameTime)
{
	if (IsActive())
	{
		float x = sin(pGameTime->GetTotalTime() * Math::PI + GetIndex());
		x *= GetSpeed() * pGameTime->GetTimeElapsed() * 1.4f;
		TranslatePosition(x, GetSpeed() * pGameTime->GetTimeElapsed());


		if (!IsOnScreen()) Deactivate();
	}

	EnemyShip::Update(pGameTime);
}


void BossEnemyShip::Draw(SpriteBatch *pSpriteBatch)
{
	if (IsActive())
	{
		pSpriteBatch->Draw(m_pTexture, GetPosition(), Color::White, m_pTexture->GetCenter(), Vector2::ONE, Math::PI, 1);
	}
}

void BossEnemyShip::AttachWeapon(Weapon *pWeapon, Vector2 position)
{
	pWeapon->SetGameObject(this);
	pWeapon->SetOffset(position);
	m_weapons.push_back(pWeapon);

	//std::cout << "\n weapon attached. Size: " << m_weapons.size() << "\n";
}

void BossEnemyShip::FireWeapons(int TextureIndexInput, TriggerType type)
{
	//std::cout << "Firing weapon\n";
	m_weaponIt = m_weapons.begin();

	//std::cout << m_weapons.size() << "\n";
	for (; m_weaponIt != m_weapons.end(); m_weaponIt++)
	{
		(*m_weaponIt)->Fire(TextureIndexInput, type);
	}
}

void BossEnemyShip::SetArrived(bool status)
{
	m_arrived = status;
}